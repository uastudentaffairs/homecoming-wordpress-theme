<?php

// Remove the main area
remove_action( 'sa_framework_start_main_area', 'sa_framework_start_main_area' );

get_header();

// Get the blog url
$bloginfo_url = get_bloginfo( 'url' );

// Set the theme directory
$sa_homecoming_dir = get_stylesheet_directory_uri();

// Get the hashtag
$hashtag = get_option( 'options_hashtag' );

?><div id="sa-homecoming-front-main">

	<div class="sa-homecoming-promo">
		<div class="row">
			<div class="small-12 columns">
				<div class="inside">
					<img class="state-logo" src="<?php echo $sa_homecoming_dir; ?>/images/forever-crimson-state-logo.png" />
					<div class="section-text">
						<div class="section-title">
							<h2>Save the Date: <span class="lighter">Sunday, <span class="show-for-small-only">10/4/15</span> <span class="show-for-medium-up">October 4</span> - Saturday, <span class="show-for-small-only">10/10/15</span> <span class="show-for-medium-up">October 10, 2015</span></span></h2>
						</div>
						<p>The 2015 SGA Homecoming Committee has been working to provide students, alumni, and fans with a week’s worth of fun and exciting events. The week will begin with the <a href="<?php echo $sa_homecoming_dir; ?>/events/roll-tide-run/">27th Annual Roll Tide Run</a> and conclude with the crowning of the <a href="<?php echo $sa_homecoming_dir; ?>/events/homecoming-queen-elections/">2015 Homecoming Queen</a> during halftime of the <a href="<?php echo $sa_homecoming_dir; ?>/events/football-game/">Alabama vs. Arkansas football game</a>. We hope you'll <a href="<?php echo $bloginfo_url; ?>/get-involved/">join in on the festivities</a> and use <strong>#<?php echo $hashtag; ?></strong> to share your experiences.</p>
					</div>
				</div>
			</div>
		</div>
	</div><?php

	// Print the events section
	print_sa_homecoming_front_events();

?></div> <!-- #sa-homecoming-front-main --><?php

get_footer();
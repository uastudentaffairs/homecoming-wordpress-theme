<?php

// Include filters
require_once( STYLESHEETPATH . '/includes/filters.php' );

//! Setup styles and scripts
add_action( 'wp_enqueue_scripts', function () {

    // Get the theme directory
    $sa_theme_dir = trailingslashit( get_stylesheet_directory_uri() );

    // Enqueue the social gathering tweets CSS
    wp_enqueue_style( 'asg-tweets', WP_PLUGIN_URL . '/a-social-gathering/css/asg-tweets.min.css' );

    // Enqueue the theme stylesheet
    wp_enqueue_style( 'sa-homecoming', $sa_theme_dir . 'css/sa-homecoming.min.css' );

    // Enqueue the theme script
    wp_enqueue_script( 'sa-homecoming', $sa_theme_dir . 'js/sa-homecoming.min.js', array( 'jquery' ), false, true );

}, 20 );

// When instagram and twitter posts are updated, reset our transient
add_action( 'save_post_asg_instagram', 'sa_homecoming_reset_social_media', 10, 3 );
add_action( 'save_post_asg_twitter', 'sa_homecoming_reset_social_media', 10, 3 );
function sa_homecoming_reset_social_media( $post_ID, $post, $update ) {

    // Reset the transient
    delete_transient( 'homecoming_front_page_social_media' );

}

// When director posts are updated, reset our transient
add_action( 'save_post_directors', function( $post_ID, $post, $update ) {

    delete_transient( 'homecoming_exec_directors' );

});

// When events posts are updated, reset our transients
add_action( 'save_post_ua_events', function( $post_ID, $post, $update ) {

    delete_transient( 'homecoming_all_events' );
    delete_transient( 'homecoming_front_page_events' );

});

// Get HTML list of social media
function get_sa_homecoming_social_media_list( $color = 'black' ) {

    // Set the framework directory
    $sa_framework_dir = get_template_directory_uri();

    // We only have 2 colors
    if ( ! in_array( $color, array( 'black', 'white' ) ) ) {
        $color = 'black';
    }

    // Get URLs
    $facebook_url = get_option( 'options_facebook_url' );
    $twitter_handle = get_option( 'options_twitter_handle' );
    $instagram_handle = get_option( 'options_instagram_handle' );

    // Build HTML
    $html = '<ul class="social-media">';

        // Add Facebook
        if ( $facebook_url ) {
            $html .= '<li class="facebook" >
                <a class="fade" href = "' . $facebook_url . '" >
                    <img class="icon" src = "' . $sa_framework_dir . '/images/icons/facebook-' . $color . '.svg" alt = "" />
                    <span class="text">Follow Homecoming on Facebook</span>
                </a >
            </li >';
        }

        // Add Twitter
        if ( $twitter_handle ) {

            // Clean up the handle
            $twitter_handle = str_replace( array( '@' ), '', $twitter_handle );

            $html .= '<li class="twitter">
                <a class="fade" href="https://twitter.com/' . $twitter_handle . '">
                    <img class="icon" src="' . $sa_framework_dir . '/images/icons/twitter-' . $color . '.svg" alt="" />
                    <span class="text">Follow Homecoming on Twitter</span>
                </a>
            </li>';
        }

        // Add Instagram
        if ( $instagram_handle ) {

            // Clean up the handle
            $instagram_handle = str_replace( array( '@' ), '', $twitter_handle );

            $html .= '<li class="instagram">
                <a class="fade" href="https://instagram.com/' . $instagram_handle . '">
                    <img class="icon" src="' . $sa_framework_dir . '/images/icons/instagram-' . $color . '.svg" alt="" />
                    <span class="text">Follow Homecoming on Instagram</span>
                </a>
            </li>';
        }

    $html .= '</ul>';

    return $html;
}

// Print the events section on the front page
function print_sa_homecoming_front_events() {
    global $post;

    // Store events in a transient
    $transient_name = 'homecoming_front_page_events';

    // Get events from transient
    $events = get_transient( $transient_name );

    // If no events in transient
    if ( false === $events || ! is_array( $events ) ) {

        // Query events
        if ( function_exists( 'get_ua_calendar_events' )
            && ( $events = get_ua_calendar_events( array( 'posts_per_page' => 4 ) ) ) ) {

            // Store the events for an hour
            set_transient( $transient_name, $events, 3600 );

        }

    }

    // Make sure we have events
    if ( ! $events->have_posts() ) {
        return false;
    }

    // Set the images folder
    $images_folder = get_stylesheet_directory_uri() . '/images/';

    ?><div class="sa-homecoming-upcoming-events">
        <div class="row">
            <div class="section-title">
                <h2>Upcoming Events <a class="view-all smaller lowercase" href="<?php echo get_bloginfo( 'url' ); ?>/events/">View All Events</a></h2>
            </div>
            <ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-4"><?php

                while ( $events->have_posts() ) :
                    $events->the_post();

                    // Get the permalink
                    $permalink = get_permalink();

                    ?><li class="sa-homecoming-event">

                        <div class="event-header">
                            <a href="<?php echo $permalink; ?>">
                                <h3 class="event-title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
                            </a>
                        </div>

                        <div class="event-text"><?php

                            if ( $post->post_excerpt ) {
                                echo wpautop( $post->post_excerpt );
                            }

                            ?><a href="<?php echo $permalink; ?>" class="button expand">Learn more about this event</a><?php

                        ?></div><?php

                        //echo "<Br />&nbsp;&nbsp;&nbsp;&nbsp; All Day: " . $event->all_day;
                        //echo "<Br />&nbsp;&nbsp;&nbsp;&nbsp; start time: " . $event->event_start_time;
                        //echo "<Br />&nbsp;&nbsp;&nbsp;&nbsp; end time: " . $event->event_end_time;

                        /*[event_date] => 20151004
                        [all_day] =>
                        [event_start_time] => 134500
                        [event_end_time] =>
                        [recurring] =>
                        [recurring_repeats] =>
                        [recurring_repeats_every] =>
                        [recurring_repeats_on] =>
                        [recurring_repeats_by] =>
                        [recurring_ends] =>
                        [recurring_ends_occurences] =>
                        [recurring_ends_on] => */

                    ?></li><?php

                endwhile;

            ?></ul>
        </div>
    </div><?php

    // Restore original Post Data
    wp_reset_postdata();

}

// ONLY FOR TESTING
add_action( 'init', function() {

    // Not in the admin
    if ( is_admin() ) {
        return;
    }

    // Only check the social media once an hour
    $transient_name = 'gather_uahomecoming_tweets';
    if ( false === get_transient( $transient_name ) ) {

        // Construct the Twitter object
        $twitter = new A_Social_Gathering_Twitter();

        // Gather the tweets
        $twitter->gather_items( 'uahomecoming', array(
            'count'           => 10,
            'exclude_replies' => true,
            'include_rts'     => true
        ) );

        // Store the gathered time for an hour
        set_transient( $transient_name, time(), 3600 );

        // Reset the social media transient
        delete_transient( 'homecoming_front_page_social_media' );

    }

});

// ONLY FOR TESTING
add_action( 'init', function() {

    // Not in the admin
    if ( is_admin() ) {
        return;
    }

    // Only check the social media once an hour
    $transient_name = 'gather_uahomecoming_instagram';
    if ( false === get_transient( $transient_name ) ) {

        // Construct the Instagram object
        $instagram = new A_Social_Gathering_Instagram();

        // Gather the posts
        $instagram->gather_items( 'uahomecoming', array(
            'count' => 10
        ) );

        // Store the gathered time for an hour
        set_transient( $transient_name, time(), 3600 );

        // Reset the social media transient
        delete_transient( 'homecoming_front_page_social_media' );

    }

});
<?php

// Set the page layout
add_filter( 'sa_framework_page_layout', function( $page_layout ) {

	// Set all events pages to full width column
	if ( is_singular( 'ua_events' ) ) {
		return '1 full width column';
	}

	return $page_layout;
});

// Print the homecoming header
add_action( 'sa_framework_print_header', 'sa_homecoming_print_header' );
function sa_homecoming_print_header() {

	// Get the blog url
	$bloginfo_url = get_bloginfo( 'url' );

	// Set the framework directory
	$sa_framework_dir = get_template_directory_uri();

	?><div id="sa-homecoming-header">
		<div class="feature-content">

			<div class="row">
				<div class="small-12 columns">

					<div class="content-bar gray-bar capitalize">
						<div class="inside">
							<div class="inside-inside">The University of Alabama Homecoming</strong> <span class="slash lighter">Oct 4 - 10, 2015</span></div>
						</div>
					</div>
					<div class="content-bar red-bar">
						<div class="inside">
							<a href="<?php echo $bloginfo_url; ?>">
								<img class="ua-square" src="<?php echo $sa_framework_dir; ?>/images/logos/ua-square.svg" alt="The University of Alabama" />
								<span class="capitalize semibold first-line"><span class="forever">Forever</span> <span class="crimson">Crimson:</span></span> <span class="lighter smaller second-line">Faithful, Loyal, Firm, and True</span>
							</a>
						</div>
					</div>
					<div id="sa-homecoming-menu-wrapper" class="content-bar black-bar capitalize">
						<div class="inside">
							<div class="inside-inside">
								<div id="sa-homecoming-menu-toggle" class="open-sa-homecoming-menu">
									<div class="menu-label">Menu</div>
									<div class="menu-label-close screen-reader-text">Close</div>
								</div>
								<ul class="menu">
									<li class="home"><a href="<?php echo $bloginfo_url; ?>">Home</a></li>
									<li class="no-slash"><a href="<?php echo $bloginfo_url; ?>/about/">About</a></li>
									<li><a href="<?php echo $bloginfo_url; ?>/get-involved/">Get Involved</a></li>
									<li><a href="<?php echo $bloginfo_url; ?>/events/">Events</a></li>
									<li><a href="<?php echo $bloginfo_url; ?>/registration/">Registration</a></li>
									<?php /*<li><a href="<?php echo $bloginfo_url; ?>/court/">Homecoming Court</a></li>
									<li><a href="#">Photos</a></li>*/ ?>
									<li><a href="http://sa.tix.com" target="_blank">Tickets</a></li>
									<li><a href="http://uagameday.com/" target="_blank">Gameday</a></li>
								</ul>
								<?php echo get_sa_homecoming_social_media_list( 'white' ); ?>
								<div class="clear"></div>
							</div>
						</div>
					</div>

				</div> <!-- .columns -->
			</div> <!-- .row -->

		</div> <!-- .feature-content -->
		<div class="arrow-down"></div>
	</div> <!-- #sa-homecoming-header --><?php

}

// Filter the page title
add_filter( 'sa_framework_page_title', function( $page_title, $post_id ) {

	// Set the page title for the about page
	if ( is_page( 'about' ) ) {
		return 'University of Alabama Homecoming';
	}

	return $page_title;

}, 100, 2 );

// Tell it to show the submenu header
add_filter( 'sa_framework_display_submenu_header', function( $display_submenu_header, $submenu_header, $menu_args ) {
	return true;
}, 100, 3 );

// Filter the main column CSS selectors
add_filter( 'sa_framework_main_column_css_selectors', function ( $selectors, $column_id ) {

	// Tells us to make the sidebar scroll with the content
	if ( in_array( 'sa-sidebar', $selectors ) && in_array( 'sa-left-sidebar', $selectors ) ) {
		$selectors = array_merge( $selectors, array( 'scroll-with-content', 'align-with-content' ) );
	}

	return $selectors;
}, 10, 2 );

// Add tweets to the sidebar
add_action( 'sa_framework_before_left_sidebar', 'sa_homecoming_print_sidebar_tweets', 10 );
function sa_homecoming_print_sidebar_tweets() {

	// Construct the social media object
	$twitter = new A_Social_Gathering_Twitter();

	// Print the tweets
	if ( $tweets = $twitter->get_printed_tweets() ) {
		?><h3 class="sidebar-header"><a href="https://twitter.com/search?q=%23UAHC15">#UAHC15</a></h3><?php
		echo $tweets;
	}

}

// Add the social media after the main area
add_action( 'sa_framework_after_main_area', 'print_sa_homecoming_social_media_footer', 100 );
function print_sa_homecoming_social_media_footer() {

	// Store events in a transient
	$transient_name = 'homecoming_front_page_social_media';

	// Get items from transient
	$social_media_items = get_transient( $transient_name );

	// Get the hashtag
	$hashtag = get_option( 'options_hashtag' );

	// Set the framework directory
	$sa_framework_dir = get_template_directory_uri();

	// If no items in transient
	if ( false === $social_media_items || ! is_array( $social_media_items ) ) {

		// Construct the social media object
		$social_media = new A_Social_Gathering();

		// Get items
		$social_media_items = $social_media->get_items( array( 'images_only' => true, 'count' => '12' ) );

		// Store the events for an hour
		set_transient( $transient_name, $social_media_items, 3600 );

	}

	// Make sure we have items
	if ( ! $social_media_items ) {
		return false;
	}

	// Build array of item HTML info
	$social_media_images = array();

	foreach( $social_media_items as $item ) {

		switch( $item->item_type ) {

			case 'instagram':

				// Make sure we have the image we need
				if ( isset( $item->media_standard ) && ! empty( $item->media_standard ) ) {

					// Set image source
					$item->image_src = $item->media_standard;

					// Add to array
					$social_media_images[] = $item;

				}

				break;

			case 'twitter':

				// Make sure we have images
				if ( isset( $item->media ) && ! empty( $item->media ) ) {

					foreach( $item->media as $media ) {

						// Make copy of item
						$item_copy = clone $item;

						// Set image source
						$item_copy->image_src = $media;

						// Add to array
						$social_media_images[] = $item_copy;

					}

				}

				break;

		}

	}

	// If we have images, build HTML
	$social_media_images_html = null;
	if ( $social_media_images ) {

		$image_index = 1;
		foreach ( $social_media_images as $image ) {

			// Make sure we have an image
			if ( ! $image->image_src ) {
				continue;
			}

			// No more than 12
			if ( $image_index > 12 ) {
				break;
			}

			// Setup classes
			$item_classes = array( 'item', $image->item_type );

			if ( $image_index > 4 && $image_index <= 6 ) {
				$item_classes[] = 'show-for-medium-up';
			} else if ( $image_index > 6 ) {
				$item_classes[] = 'show-for-large-up';
			}

			// Build the item's HTML
			$social_media_images_html .= '<div class="' . implode( ' ', $item_classes ) . '">
                <div class="item-image" style="background-image:url(\'' . $image->image_src . '\');">';

				// Add the permalink
				if ( $image->permalink ) {
					$social_media_images_html .= '<a href="' . $image->permalink . '" target="_blank"></a>';
				}

			// Close the item's HTML
			$social_media_images_html .= '</div>
                </div>';

			// Keep track of images
			$image_index++;

		}

	}

	// Print all the HTML
	if ( $social_media_images_html ) {

		// Get URLs
		$facebook_url = get_option( 'options_facebook_url' );
		$twitter_handle = str_replace( array( '@' ), '', get_option( 'options_twitter_handle' ) );
		$instagram_handle = str_replace( array( '@' ), '', get_option( 'options_instagram_handle' ) );

		?><div id="sa-homecoming-social-media-footer">
			<div class="row">
				<div class="small-12 columns">
					<div class="sa-homecoming-social-media">
						<?php echo $social_media_images_html; ?>
						<div class="section-title-wrapper">
							<div class="section-title-inside">
								<div class="section-title">
									<a class="fade social-media-icon facebook" href="<?php echo $facebook_url; ?>" target="_blank"><img class="icon" src="<?php echo $sa_framework_dir; ?>/images/icons/facebook-white.svg"><span class="text">Follow UA Homecoming on Facebook</span></a>
									<a class="fade social-media-icon twitter" href="https://twitter.com/<?php echo $twitter_handle; ?>" target="_blank"><img class="icon" src="<?php echo $sa_framework_dir; ?>/images/icons/twitter-white.svg"><span class="text">Follow UA Homecoming on Twitter</span></a>
									<a class="fade social-media-icon instagram" href="http://instagram.com/<?php echo $instagram_handle; ?>" target="_blank"><img class="icon" src="<?php echo $sa_framework_dir; ?>/images/icons/instagram-white.svg""><span class="text">Follow UA Homecoming on Instagram</span></a>
									<h2 class="section-header">See What's Happening <span class="hashtag">#<?php echo $hashtag; ?></span></h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><?php

	}

}
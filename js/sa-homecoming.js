(function( $ ) {
    'use strict';

    // Setup menu to "full screen"
    var $sa_homecoming_menu_wrapper  = $( '#sa-homecoming-menu-wrapper' );
    var $sa_homecoming_open_menu = $( '.open-sa-homecoming-menu' );
    $sa_homecoming_open_menu.on( 'click touchstart', function( $event ) {
        $event.preventDefault();

        // If it's already "full screen", close it
        if ( $sa_homecoming_menu_wrapper.hasClass( 'full-screen' ) ) {
            sa_homecoming_close_full_screen_menu();
        }

        // Otherwise open the full screen menu
        else {
            sa_homecoming_open_full_screen_menu();
        }

    });

    ////////// FUNCTIONS //////////

    // Open the full screen menu
    function sa_homecoming_open_full_screen_menu() {

        $sa_homecoming_menu_wrapper.addClass( 'full-screen');
        $('html').addClass( 'no-overflow' );
        $('body').addClass( 'has-full-screen-menu' );

        // Setup keydown event
        $( document ).on( 'keydown', function($event) {

            // Run code according to the key
            switch( $event.which ) {

                // ESC key
                case 27:

                    // Close the full screen menu search
                    sa_homecoming_close_full_screen_menu();

                    break;

            }

        });

    }

    // Close the full screen menu
    function sa_homecoming_close_full_screen_menu() {

        $sa_homecoming_menu_wrapper.removeClass( 'full-screen' );
        $('html').removeClass( 'no-overflow');
        $('body').removeClass( 'has-full-screen-menu' );

    }

})( jQuery );
<?php

// Template Name: Homecoming Directors

// Add our directors after any content
add_action( 'sa_framework_after_content', function() {

	// Cache HTML in a transient - is flushed whenever a director item is updated
	$transient_name = 'homecoming_exec_directors';

	// Get HTML from the transient
	$directors_html = get_transient( $transient_name );

	// If no HTML in transient
	if ( false === $directors_html || $directors_html == '' ) {

		// Build HTML
		$directors_html = null;

		// Get the directors
		$the_directors = get_posts( array(
			'post_type' => 'directors',
			'posts_per_page' => '-1',
			'orderby' => 'menu_order',
			'order' => 'ASC',
		) );

		// Will hold all directors indexed by type
		$all_directors = array();

		if ( $the_directors ) {
			foreach ( $the_directors as $director ) {

				// Get the director type
				$director_type = get_post_meta( $director->ID, 'director_type', true );

				if ( ! $director_type ) {
					$director_type = 'Associate Directors';
				}

				// Store in director object
				$director->director_type = $director_type;

				// Add to array
				$director_type_index = strtolower( preg_replace( '/[^a-z0-9]/i', '_', $director_type ) );
				$all_directors[ $director_type_index ][] = $director;

			}
		}

		// Print the directors
		if ( $all_directors ) {
			foreach( array( 'executive_director', 'faculty_advisor', 'associate_director' ) as $director_type ) {

				// Get these directors
				$directors = isset( $all_directors[ $director_type ] ) ? $all_directors[ $director_type ] : false;

				// Only if we have directors
				if ( ! $directors ) {
					continue;
				}

				switch( $director_type ) {

					case 'executive_director':
						$directors_html .= '<h2>Executive Director' . ( count( $directors ) > 1 ? 's' : null ) . '</h2>';
						break;

					case 'faculty_advisor':
						$directors_html .= '<h2>Faculty Advisor' . ( count( $directors ) > 1 ? 's' : null ) . '</h2>';
						break;

					case 'associate_director':
						$directors_html .= '<h2>Associate Director' . ( count( $directors ) > 1 ? 's' : null ) . '</h2>';
						break;

				}

				// Will hold this group's HTML
				$this_director_group = array();

				foreach( $directors as $director ) {

					// Get name
					$first_name = get_post_meta( $director->ID, 'first_name', true );
					$last_name = get_post_meta( $director->ID, 'last_name', true );

					// Make sure we have a name
					if ( ! ( $first_name && $last_name ) ) {
						continue;
					}

					// Create director HTML
					$this_director_html = null;

					// Get director title
					$director_title = ( 'associate_director' == $director_type ) ? get_post_meta( $director->ID, 'director_title', true ) : false;

					// Add title
					if ( $director_title ) {

						if ( 'associate_director' == $director_type ) {
							$this_director_html .= "<strong>{$director_title}</strong><br />";
						} else {
							$this_director_html .= "{$director_title}<br />";
						}


					}

					// Add name
					if ( $first_name && $last_name ) {

						if ( 'associate_director' != $director_type ) {
							$this_director_html .= "<strong>{$first_name} {$last_name}</strong>";
						} else {
							$this_director_html .= "{$first_name} {$last_name}";
						}

					}

					// Get/add email
					$email = antispambot( get_post_meta( $director->ID, 'email', true ) );
					if ( $email ) {
						$this_director_html .= '<br /><a href="mailto:' . $email . '">' . $email . '</a>';
					}

					// Get/add phone
					$phone = get_post_meta( $director->ID, 'phone', true );
					if ( $phone ) {
						$this_director_html .= '<br />' . $phone;
					}

					// Add director to group HTML
					$this_director_group[] = '<p>' . $this_director_html . '</p>';

				}

				// Wrap group in columns if need be
				if ( count( $this_director_group ) > 1 ) {
					$directors_html .= '<div class="row"><div class="small-12 medium-6 columns">' . implode( '</div><div class="small-12 medium-6 columns">', $this_director_group ) . '</div></div>';
				} else {
					$directors_html .= implode( '', $this_director_group );
				}

			}
		}

		// Store the HTML for 24 hours
		set_transient( $transient_name, $directors_html, $directors_html );

	}

	// Print the HTML
	echo $directors_html;

});

get_header();

get_footer();
<?php

// Template Name: Homecoming Events

get_header();

// Store events in a transient
$transient_name = 'homecoming_all_events';

// Get events from transient
$events = get_transient( $transient_name );

// If no events in transient
if ( false === $events || ! is_array( $events ) ) {

	// Query events
	if ( function_exists( 'get_ua_calendar_events' )
		&& ( $events = get_ua_calendar_events( array( 'posts_per_page' => -1, 'view' => 'all' ) ) )
		&& $events->have_posts() ) {

		// Store posts
		$events = $events->posts;

		$events_by_date = array();
		foreach ( $events as $event ) {

			// Get event date
			$event_date = isset( $event->event_date ) ? $event->event_date : false;

			// No point if no date
			if ( ! $event->event_date ) {
				continue;
			}

			// Add to array
			$events_by_date[ $event_date ][] = $event;

		}

		if ( empty( $events_by_date ) ) {
			$events = null;
		} else {
			$events = $events_by_date;
		}

	}

	// Store the events for an hour
	set_transient( $transient_name, $events, 3600 );

}

// Make sure we have events
if ( ! $events ) {
	return false;
}

?><div class="sa-homecoming-events-wrapper">
	<div class="row">
		<div class="small-12 columns">
			<div class="sa-homecoming-events"><?php

			// Print out the events by day
			foreach( $events as $date => $day_events ) {

				// Convert the date
				$date = new DateTime( $date, new DateTimeZone( 'America/Chicago' ) );

				?><div class="event-day">
					<h2 class="date-title"><?php echo $date->format( '\<\s\t\r\o\n\g\>l\<\/\s\t\r\o\n\g\> \/ F j, Y' ); ?></h2>
					<div class="clear"></div><?php

					foreach( $day_events as $event ) {

						?><div class="event">
							<h3 class="event-title"><a href="<?php echo get_permalink( $event->ID ); ?>"><?php echo get_the_title( $event->ID ); ?></a></h3><?php

							if ( $event->post_excerpt ) {
								echo wpautop( $event->post_excerpt );
							}

						?></div> <!-- .event --><?php

					}

					?><div class="clear"></div>
				</div> <!-- .event-day --><?php

			}

			// Reset the postdata
			//wp_reset_postdata();

			?></div> <!-- .sa-homecoming-events -->
		</div> <!-- .columns -->
	</div> <!-- .row -->
</div> <!-- .sa-homecoming-events-wrapper --><?php

get_footer();